<?php

namespace Drupal\shortcode_svg\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\FileStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller routines for svg icons from sprite.
 */
class ShortcodeSvgController extends ControllerBase {
  /**
   * Request injected.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * File injected.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * File url injected.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container pulled in.
   *
   * @return static
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('request_stack'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Drupal\file\FileStorageInterface $file_storage
   *   The file storage backend.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator.
   */
  public function __construct(RequestStack $request_stack, FileStorageInterface $file_storage, FileUrlGeneratorInterface $file_url_generator) {
    $this->request = $request_stack;
    $this->fileStorage = $file_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * Routes for svg icons.
   */
  public function content() {
    $config = $this->config('shortcode_svg.settings');
    $fid = $config->get('image');
    $colors = $config->get('colors');
    $width = $config->get('width');
    $color = [];
    if (isset($colors)) {
      $color_lines = explode(PHP_EOL, $colors);
      foreach ($color_lines as $color_line) {
        if (!empty(rtrim($color_line))) {
          $parts = explode(':', $color_line);
          $name = trim($parts[0]);
          $hex = trim($parts[1]);
          $color[$hex] = $name;
        }
      }
    }
    $file = $this->fileStorage->load($fid);
    $path = $this->fileUrlGenerator->generateString($file->getFileUri());
    // Remove initial / in path.
    $xml_path = substr($path, 1);
    $xml = new \XMLReader();
    $xml->open($xml_path);
    $ids = [];
    while ($xml->read()) {
      if ($xml->nodeType === \XMLReader::ELEMENT && $xml->name == 'g' && $xml->getAttribute('id')) {
        $ids[] = $xml->getAttribute('id');
      }
    }
    $content = '<h4>Icons</h4><ul class="svg_list">';
    foreach ($ids as $icon_name) {
      $content .= sprintf(
        '<li id="%s"><svg viewBox="0 0 34 34" class="icon">
        <use xlink:href="%s#%s"></use>
        </svg></li>',
      $icon_name,
      $path,
      $icon_name
      );
    }
    $content .= '</ul><div class="fixed-bottom"><button type="button" class="copy-icon" data-clipboard="I like turtles">Copy shortcode to clipboard</button></div>';

    $form['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image title'),
      '#maxlength' => 100,
      '#suffix' => $this->t('Optional alt text for the icon. Only use if necessary for someone to understand the context of the icon.'),
      '#attributes' => [
        'class' => [
          'svg-alt',
        ],
      ],
    ];

    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#value' => $width,
      '#maxlength' => 10,
      '#suffix' => $this->t('Only place number into this box to set the width of the icon'),
      '#attributes' => [
        'class' => [
          'svg-width',
        ],
      ],
    ];

    $form['colors'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#options' => $color,
      '#attributes' => [
        'class' => [
          'svg-color',
        ],
      ],
    ];

    $form['icon_list'] = [
      '#markup' => $content,
      '#allowed_tags' => [
        'div',
        'p',
        'h4',
        'strong',
        'svg',
        'use',
        'ul',
        'li',
        'button',
      ],
    ];
    $form['#attached']['library'][] = 'shortcode_svg/icon_form';
    $form['#attached']['library'][] = 'shortcode_svg/clipboard';

    return $form;
  }

}
