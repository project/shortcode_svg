<?php

namespace Drupal\shortcode_svg\Plugin\Shortcode;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Language\Language;
use Drupal\Core\Render\RendererInterface;
use Drupal\shortcode\Plugin\ShortcodeBase;
use Drupal\shortcode_svg\Plugin\ShortcodeIcon;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a shortcode for SVG image sprite.
 *
 * @Shortcode(
 *   id = "svg",
 *   title = @Translation("Svg Shortcode"),
 *   description = @Translation("Svg shortcode")
 * )
 */
class SvgShortcode extends ShortcodeBase {

  /**
   * Call shortcode svg icon.
   *
   * @var \Drupal\shortcode_svg\Plugin\ShortcodeIcon
   */
  protected $shortcodeSvgIcon;

  /**
   * Constructs a new Shortcode plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\shortcode_svg\Plugin\ShortcodeIcon $shortcode_svg_icon
   *   Shortcode svg icon.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    RendererInterface $renderer,
    ShortcodeIcon $shortcode_svg_icon
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $renderer);
    $this->shortcodeSvgIcon = $shortcode_svg_icon;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('shortcode_svg.icon')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attributes = $this->getAttributes(
      [
        'name' => '',
        'alt' => '',
        'width' => '',
        'color' => '',
      ],
      $attributes
    );

    $name = !empty($attributes['name']) ? Xss::filter($attributes['name']) : NULL;
    $content = '';

    if ($name !== NULL) {
      $alt = !empty($attributes['alt']) ? Xss::filter($attributes['alt']) : NULL;
      $title = "";
      $label = "";
      if ($alt !== NULL) {
        $title = "<title id='svg_title'>$alt</title>";
        $label = "aria-labelledby='svg_title'";
      }
      $width = !empty($attributes['width']) ? Xss::filter($attributes['width']) : NULL;
      $color = !empty($attributes['color']) ? Xss::filter($attributes['color']) : NULL;

      $icon = $this->shortcodeSvgIcon;
      $path = $icon->getSvg();

      $content = sprintf(
        '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34" %s class="svg-icon %s" width="%s">
        %s<use fill="#%s" xlink:href="%s#%s"></use>
        </svg>',
      $label,
        $name,
        $width,
        $title,
        $color,
        $path,
        $name
      );
    }

    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $output = [];
    $output[] = '<p><strong>' . $this->t('[svg name="icon_name" alt="optional description" width="number only" color="hex code no #"][/svg]') . '</strong> ';

    return implode(' ', $output);
  }

}
