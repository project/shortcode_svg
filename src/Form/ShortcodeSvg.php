<?php

namespace Drupal\shortcode_svg\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\FileStorageInterface;
use enshrined\svgSanitize\Sanitizer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Upload SVG sprite.
 *
 * @ingroup shortcode_svg
 */
class ShortcodeSvg extends ConfigFormBase {

  /**
   * File injected.
   *
   * @var \Drupal\file\FileStorageInterface
   */
  protected $fileStorage;

  /**
   * File url injected.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\file\FileStorageInterface $file_storage
   *   The file storage backend.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator.
   */
  public function __construct(
      FileStorageInterface $file_storage,
      FileUrlGeneratorInterface $file_url_generator
      ) {
    $this->fileStorage = $file_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container pulled in.
   *
   * @return self
   *   Return self.
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'render_shortcode_svg_form';
  }

  /**
   * {@inheritdoc}
   *
   * @return string[]
   *   Return array of config names.
   */
  public function getEditableConfigNames() {
    return [
      'shortcode_svg.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('shortcode_svg.settings');
    $stored_image = NULL !== $config->get('image') ? $config->get('image') : '';
    $colors = NULL !== $config->get('colors') ? $config->get('colors') : '';
    $width = NULL !== $config->get('width') ? $config->get('width') : '';

    $url = Url::fromRoute('shortcode_svg.list');
    $icon_link = Link::fromTextAndUrl($this->t('on this page'), $url)->toString();
    $description = sprintf(
      '%s. %s %s.',
      $this->t("Upload a layered svg file below, with each icon having it's own selectable id"),
      $this->t('You can view your icons'),
      $icon_link
    );

    $form['svg_description'] = [
      '#markup' => $description,
    ];

    $form['svg_image'] = [
      '#title' => $this->t('Svg Image'),
      '#type' => 'managed_file',
      '#description' => $this->t('Upload SVG image here'),
      '#upload_location' => 'public://svg/',
      '#upload_validators' => [
        'file_validate_extensions' => ['svg'],
        'file_validate_size' => [3000000],
      ],
      '#default_value' => [$stored_image],
    ];

    $form['svg_colors'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Icon Colors"),
      '#description' => $this->t('Create a dropdown with your site colors. List out the colors you want available, one per line. Write as color colon hex code name as follows <br />  white: ffffff'),
      '#default_value' => $colors,
    ];

    $form['svg_width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Width'),
      '#description' => $this->t('Default icon width'),
      '#default_value' => $width,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Implements form validation.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sanitizer = new Sanitizer();
    $image = Xss::filter($form_state->getValue('svg_image')[0]);
    $colors = Xss::filter($form_state->getValue('svg_colors'));
    $width = Xss::filter($form_state->getValue('svg_width'));
    $file = $this->fileStorage->load($image);
    $uri = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());
    $image_path = $this->fileUrlGenerator->generateString($file->getFileUri());
    $svg = file_get_contents($uri);
    // Sanitize SVG text.
    $cleanSVG = $sanitizer->sanitize($svg);
    $f = fopen($_SERVER['DOCUMENT_ROOT'] . $image_path, 'w');
    fwrite($f, $cleanSVG);
    fclose($f);
    // Overwrite uploaded file with sanitized file.
    $file->setPermanent();
    $file->save();
    // Retrieve the configuration.
    $this->configFactory->getEditable('shortcode_svg.settings')
      ->set('image', $file->id())
      ->set('colors', $colors)
      ->set('width', $width)
      ->save();

    parent::submitForm($form, $form_state);
  }

}
