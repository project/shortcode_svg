<?php

namespace Drupal\shortcode_svg\Plugin;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Provides svg icon location and code for usage outside shortcode.
 *
 * @ShortcodeIcon(
 *   id = "ShortcodeIcon",
 *   title = @Translation("Shortcode Icon"),
 *   description = @Translation("Path/usage of shortcode icon")
 * )
 */
class ShortcodeIcon {

  /**
   * The storage handler class for files.
   *
   * @var \Drupal\file\FileStorage
   */
  private $fileStorage;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * File Url Generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;


  /**
   * Image Location.
   *
   * @var string
   */
  private $svgLocation;

  /**
   * Load icon location.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    FileUrlGeneratorInterface $file_url_generator,
    EntityTypeManagerInterface $entity
    ) {
    $this->configFactory = $config_factory;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileStorage = $entity->getStorage('file');

    $fid = $this->configFactory->get('shortcode_svg.settings')->get('image');

    if ($fid == '') {
      $this->svgLocation = '';
    }
    else {
      $file = $this->fileStorage->load($fid);
      $path = $file != NULL ? $this->fileUrlGenerator->transformRelative($this->fileUrlGenerator->generateAbsoluteString($file->getFileUri())) : NULL;
      $this->svgLocation = $path ?? '';
    }
  }

  /**
   * Function to output icon.
   */
  public function setIcon($icon, $width, $color) {
    $path = $this->getSvg();
    if ($path == '') {
      return '';
    }
    else {
      return "<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 34 34' class='svg-icon $icon' width='$width'>
        <use fill='$color' xlink:href='$path#$icon'></use>
        </svg>";
    }
  }

  /**
   * Return Location.
   *
   * @return string
   *   Icon location.
   */
  public function getSvg() {
    return $this->svgLocation;
  }

}
